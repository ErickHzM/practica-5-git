package mx.unitec.moviles.practica5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import mx.unitec.moviles.practica5.service.MyJobintentService

class JobActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job)

        val intent = Intent(this, MyJobintentService::class.java)
        intent.putExtra("max", 100)
        MyJobintentService.enqueuework(this, intent)
    }
}
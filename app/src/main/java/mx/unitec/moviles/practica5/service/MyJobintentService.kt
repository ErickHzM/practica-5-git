package mx.unitec.moviles.practica5.service

import android.content.Context
import android.util.Log
import android.content.Intent
import androidx.core.app.JobIntentService

class MyJobintentService: JobIntentService() {
    val TAG = "MyFirtsJob"
    override fun onHandleWork(intent: Intent) {
        val max = intent.getIntExtra("max", -1)
        for (i in 0 until max){
            Log.d(TAG, "working: Numero $i")
        }
        try{
            Thread.sleep(1000)
        } catch (e: InterruptedException){
            e.printStackTrace()
        }
    }

    companion object {
        private const val JOB_ID = 2

        fun enqueueWork (context: Context, intent: Intent){
            enqueueWork(context, MyJobintentService::class.java, JOB_ID, intent)
        }
    }
}